# B2W Challenge - Resize Photos

### Prerequisites :

    1. Unix Shell
    2. Python >= 2.7 
    3. MongoDB >= 3.4.9
    4. Python modules
        4. pip install -r requirements.txt


### Running:

    You can run this code doing:
        1. cd desafio_b2w_resize_photo
        2. python simple_webservice.py

            3. Collect images from Webservice endpoint (http://54.152.221.29/images.json)

                Access : http://127.0.0.1:5000/collect_images

            4. List images info (json format)

                Access : http://127.0.0.1:5000/images

            5. Download an image

                Access : http://127.0.0.1:5000/images/<image_name>


### Tests:
    
    You can run tests doing:
        1. cd desafio_b2w_resize_photo
        2. python -m unittest discover