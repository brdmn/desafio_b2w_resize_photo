#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import requests

from PIL import Image
from StringIO import StringIO


class ImageTool():

    '''
    This class is used to manipulate images.
    '''

    def __init__(self, img):

        self.image = Image.open(StringIO(img))

    def resize_image(self, width, height):
        '''
        This method is used to resize an image.

        Keywords:
            width - New image width
            height - New image height
        '''

        self.img_size = {'width': width, 'height': height}

        self.image = self.image.resize([width, height])

        return self.image

    def save_image(self, file_folder, file_name, file_format):
        '''
        This method is used to save an image in a file.

        Keywords:
            file_folder - Folder where image will be saved
            file_name - Name of image file
            file_format - Format of image file ("JPEG", "PNG" ...)
        '''

        self.img_name = file_name
        self.img_url = file_folder + file_name

        self.image.save(self.img_url, format=file_format)

        return

    def __del__(self):
        return


if __name__ == '__main__':

    # Images URL
    URL = "http://54.152.221.29/images.json"

    # Folder where images are stored
    IMG_FOLDER = "./images/"

    IMG_FORMAT = "JPEG"

    # Images size
    IMG_SIZES = {'small': {'width': 320, 'height': 240},
                 'medium': {'width': 384, 'height': 288},
                 'large': {'width': 640, 'height': 480}
                 }

    photos = json.loads(requests.get(URL).text)

    for img in photos['images']:

        img_url = img['url']
        img_name = img_url.split("/")[-1]
        img_obj = requests.get(img_url).content

        for size in IMG_SIZES.keys():

            new_image = ImageTool(img_obj)

            new_img_width = IMG_SIZES[size]['width']
            new_img_height = IMG_SIZES[size]['height']
            new_img_name = '{0}_{1}'.format(size, img_name)

            new_image.resize_image(new_img_width, new_img_height)
            new_image.save_image(IMG_FOLDER, new_img_name, IMG_FORMAT)

            del new_image

    print("Sucess!")

