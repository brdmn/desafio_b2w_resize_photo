#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import requests

from flask import Flask
from flask import jsonify
from flask import request
from flask import send_file
from flask_pymongo import PyMongo

from libs.resize_photos import ImageTool


app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'imagesDB'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/imagesDB'

mongo = PyMongo(app)


@app.route('/images', methods=['GET'])
def list_all_images():
    '''
    Return a json containing images info
    '''

    output = []

    images = mongo.db.images
    for img in images.find():
        output.append({'filename': img['filename'], 'url': img['url'],
                       'size': img['size']})

    return jsonify({'result': output})


@app.route('/images/<image_name>', methods=['GET'])
def get_image(image_name):
    '''
    Given an image name, return the image file

    keywords:
        image_name - Image name
    '''

    filename = '{0}{1}'.format('./images/', image_name)

    return send_file(filename, mimetype='image/jpg')


@app.route('/collect_images', methods=['GET'])
def collect_all_images():
    '''
    Given a Webservice endpoint(http://54.152.221.29/images.json),
    that returns a JSON of ten photos, consume it and generate
    three different photo formats for each one, that must be small (320x240),
    medium (384x288) and large (640x480). All photos are saved in /images/
    folder and images info in MongoDB
    '''

    # Mongo Connector
    images = mongo.db.images

    # Webservice endpoint
    URL = "http://54.152.221.29/images.json"

    IMG_FORMAT = "JPEG"

    # Image sizes
    IMG_SIZES = {'small': {'width': 320, 'height': 240},
                 'medium': {'width': 384, 'height': 288},
                 'large': {'width': 640, 'height': 480}
                 }

    photos = json.loads(requests.get(URL).text)

    # Generate three different photo files.
    for img in photos['images']:

        img_url = img['url']
        img_name = img_url.split("/")[-1]
        img_obj = requests.get(img_url).content

        for size in IMG_SIZES.keys():

            new_image = ImageTool(img_obj)

            new_img_name = '{0}_{1}'.format(size, img_name)
            new_img_size = {"width": IMG_SIZES[size]['width'],
                            'height': IMG_SIZES[size]['height']}

            new_image_url = '{0}{1}{2}'.format(
                request.url_root, 'images/', new_img_name)

            new_image.resize_image(
                new_img_size['width'], new_img_size['height'])

            new_image.save_image('./images/', new_img_name, IMG_FORMAT)

            images.insert({'filename': new_img_name, 'url': new_image_url,
                           'size': new_img_size})

            del new_image

    return jsonify({'result': "Success! All images were collected."})


if __name__ == '__main__':
    app.run(debug=True)
